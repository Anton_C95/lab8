module javaGUI {
	requires java.desktop;
	requires java.logging;
	requires java.base;
	requires java.instrument;
	requires java.management;
}