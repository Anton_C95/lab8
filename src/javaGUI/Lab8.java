package javaGUI;
import javax.swing.JFrame;

import javax.swing.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicLong;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

public class Lab8 {
	static JLabel l1;  // "Primes in ..."
	JLabel l2;  // "Primes found ..."
	JTextArea textArea;
	JScrollPane scrollV;
	static JProgressBar progressBar;
	JPanel center;
	JPanel lower;
	JButton start;
	JButton cancel;
	static long number = 0;
	static long chunkSize = 0;
	static AtomicLong chunks;  // Total number of chunks
	static AtomicLong primesFound;  
	static AtomicLong chunksCompleted;  // Used to determine progress
	List<SwingWorker<Void, Long>> swingWorkers = new ArrayList<SwingWorker<Void, Long>>();

	public static void main(String[] args) {
		try {
			number = Long.parseLong(args[0]);
			chunkSize = Long.parseLong(args[1]);
			if(chunkSize < 1 || chunkSize > number || number < 1) {
				throw new java.lang.IllegalArgumentException();
			}
		} catch (java.lang.NumberFormatException e) {
			System.out.println(e.toString());
			System.exit(1);
		} catch (java.lang.IllegalArgumentException e) {
			System.out.println(e.toString());
			System.exit(1);
		}
		Lab8 app = new Lab8();
		app.go();
	}

	public boolean isPrime(long number) {
		long limit = (long) Math.sqrt(number)+1;
		for(long i=2;i<limit;++i)
			if((number%i)==0)
				return false;
		return true;
	}

	/// Sets up the components and adds listeners to the start and cancel buttons.
	public void go() {
		JFrame frame = new JFrame("PrimeSeeker");
		primesFound = new AtomicLong(0);
		chunksCompleted = new AtomicLong(0);
		chunks = new AtomicLong(0);

		textArea = new JTextArea ();
		textArea = new JTextArea(8,41);
		textArea.setLineWrap(true);
		textArea.setWrapStyleWord(true);
		textArea.setEditable(false);


		l1 = new JLabel("Primes in");  

		scrollV = new JScrollPane (textArea);
		scrollV.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

		progressBar = new JProgressBar(0, 100);
		progressBar.setValue(0);
		progressBar.setStringPainted(true);

		center = new JPanel(new BorderLayout());
		center.add(BorderLayout.NORTH, l1);
		center.add(BorderLayout.CENTER, scrollV);
		center.add(BorderLayout.SOUTH, progressBar);
		frame.getContentPane().add(BorderLayout.CENTER, center);


		lower = new JPanel(new FlowLayout(FlowLayout.LEFT)); 
		start = new JButton("Start");
		cancel = new JButton("Cancel");
		l2 = new JLabel("Primes found: "); 
		lower.add(start);
		lower.add(cancel);
		lower.add(l2);
		frame.getContentPane().add(BorderLayout.SOUTH, lower);

		frame.setSize(500, 300);
		frame.setVisible(true);

		start.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				SwingWorker<Void, Void> starter = new startSwingWorkers();
				starter.execute();
				start.setEnabled(false);
			} 
		} );

		cancel.addActionListener(new ActionListener() { 
			public void actionPerformed(ActionEvent e) { 
				/*SwingWorker<Void, Void> cancelWorkers = new cancelSwingWorkers();
				cancelWorkers.execute();
				for(Integer i = swingWorkers.size() -1; i > -1; i--) {  // Iterating through the list backwards so as not to play catch up with the executing threads and thereby catching the threads before the execute.
					swingWorkers.get(i).cancel(true);
				}*/
				Runnable cancelTask = () -> {
					for(Integer i = swingWorkers.size() -1; i > -1; i--) {  // Iterating through the list backwards so as not to play catch up with the executing threads and thereby catching the threads before the execute.
						swingWorkers.get(i).cancel(true);
					}
				};  
				Thread cancelThread = new Thread(cancelTask);
				cancelThread.start();
				cancel.setEnabled(false);
			} 
		} );
	}

	/// Event handling for start button
	class startSwingWorkers extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			int nextIntervalStart = 2;
			while(nextIntervalStart + chunkSize <= number) {
				SwingWorker<Void, Long> task = new PrimeNumbersTask(nextIntervalStart, nextIntervalStart + chunkSize, number, primesFound, chunks, chunksCompleted);
				task.addPropertyChangeListener(new progressListener());
				swingWorkers.add(task);
				nextIntervalStart += chunkSize + 1;
				chunks.incrementAndGet();
			}
			if (nextIntervalStart != number + 1 && nextIntervalStart + chunkSize > number) {
				SwingWorker<Void, Long> task = new PrimeNumbersTask(nextIntervalStart, number, number, primesFound, chunks, chunksCompleted);
				task.addPropertyChangeListener(new progressListener());
				swingWorkers.add(task);
				chunks.incrementAndGet();
			}	
			for(SwingWorker<Void, Long> worker: swingWorkers) {
				worker.execute();
			}
			return null;
		}

	}

	/// Event handling for cancel button
	class cancelSwingWorkers extends SwingWorker<Void, Void> {

		@Override
		protected Void doInBackground() throws Exception {
			for(Integer i = swingWorkers.size() -1; i > -1; i--) {
				swingWorkers.get(i).cancel(true);
			}
			return null;
		}
	}

	// Find the prime numbers in the allotted chunk and update the gui
	class PrimeNumbersTask extends
	SwingWorker<Void, Long> {
		long start;
		long end;
		long number;

		PrimeNumbersTask(long start, long end, long number, AtomicLong primesFound, AtomicLong chunks, AtomicLong chunksCompleted) {
			this.start = start;
			this.end = end;
			this.number = number; 
		}

		@Override
		protected void done() {
			setProgress((int) (chunksCompleted.incrementAndGet()/chunks.get() * 100));
			l2.setText("Primes found: " + primesFound);  // Commented out because it freezes gui
			super.done();
		}

		@Override
		protected Void doInBackground() throws Exception {
			for (long n = start; n <= end; n++) {
				if(isPrime(n)) {
					publish(n);
					primesFound.incrementAndGet();
				}
			}
			return null;
		}

		@Override
		protected void process(List<Long> chunks) {
			for (Long number : chunks) {
				textArea.append(number + ",");  // Commented out because it freezes gui
			}
		}
	}

	static class progressListener implements PropertyChangeListener {
		@Override
		public void propertyChange(PropertyChangeEvent e) {
			if ("progress".equals(e.getPropertyName())) {
				progressBar.setValue((int) e.getNewValue());
			}
		}
	}
}